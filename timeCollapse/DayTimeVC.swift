//
//  DayTimeVC.swift
//  Organizer
//
//  Created by Katok on 26.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit

class DayTimeVC: UITableViewController {
    
    
    @IBAction func addNewEvent(_ sender: UIBarButtonItem) {
        
    }
    var day: [String]  = ["07:00", "08:00"]
    
    var receivedData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        print(receivedData)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    
    // day sheduled
    override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return day.count
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDay = tableView.dequeueReusableCell(withIdentifier: "cellDay", for: indexPath)
        cellDay.textLabel?.text = "\(day[indexPath.row])"
        return cellDay
        
    }
}

