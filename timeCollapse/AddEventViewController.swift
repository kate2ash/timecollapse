////
////  CalendarEvents.swift
////  Organizer
////
////  Created by Katok on 27.02.17.
////  Copyright © 2017 Katok. All rights reserved.
////
//import EventKit
//import UIKit
//class AddEventViewController: UIViewController, UITableViewDelegate {
//    
//    var calendar: EKCalendar! // Intvared to be set in parent controller's prepareForSegue event
//    
//    let eventStore = EKEventStore()
//    //var delegate: EventAddedDelegate?
//    
//    
//    @IBOutlet var clientsNameTextField: UITextField!
//    @IBOutlet var eventDatePicker: UIDatePicker!
//    @IBOutlet var addAndSendRequestForEvent: UIButton!
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//        self.eventDatePicker.setDate(initialDatePickerValue(), animated: false)
//    }
//    
//    func initialDatePickerValue() -> Date {
//        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute]
//        
//        var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
//        
//        dateComponents.hour = 0
//        dateComponents.minute = 0
//
//        
//        return Calendar.current.date(from: dateComponents)!
//    }
//    
//    @IBAction func cancelButtonTapped(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    @IBAction func addEventButtonTapped(_ sender: UIButton) {
//        
//        
//        if let dataMVC = navigationController!.viewControllers[navigationController!.viewControllers.count - 2] as? DayTimeVC {
//            dataMVC.title = clientsNameTextField.text
//            dataMVC.day[1] = "HH:mm"
//        }
//        
//        _ = navigationController?.popViewController(animated: true)
//
//    }
//}
