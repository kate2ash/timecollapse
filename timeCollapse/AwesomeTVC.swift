//
//  ViewController.swift
//  Organizer
//
//  Created by Katok on 25.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit
import EventKit
import Foundation

class AwesomeTVC: UITableViewController {
    
    var weekList: [String] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    var dayTime = ["08:00", "09:00", "10:00", "11:00"]
    var date = Date()
    var calendar = Calendar.current
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let timeFormatter = DateFormatter()
        var dateComponents = DateComponents()
        let minutes = dateComponents.minute
        let hour = dateComponents.hour
        
        date = dateComponents.date!
        print(timeFormatter.string(from: date))
         print("hours = \(hour):\(minutes)")
      
        let time: Set<Calendar.Component> = [.hour, .minute]
        
        print(time)
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
}
    
    // MARK: - Table view data sourc
    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return weekList.count
//    }
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//        cell.textLabel?.text = "\(indexPath.row.description)\t|\t\(weekList[indexPath.row])"
//        return cell
//    }
//    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath)
//        // Segue to the second view controller
//        self.performSegue(withIdentifier: "test", sender: self)
//    }
//    
//    // passing data from AwesomeTVC to DayTimeVC
//   override func prepare (for segue: UIStoryboardSegue, sender: Any?) {
//        if let cell = sender as? UITableViewCell {
//            if let IndexPath = self.tableView.indexPath(for: cell) {
//                let element = dayTime[IndexPath.row]
//                print(IndexPath, element)
//                segue.destination.title = element
//            }
//            }
//        }
//}
//}

