//
//  week.swift
//  Organizer
//
//  Created by Katok on 25.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit

class Weeks: NSObject {
    
    
    let quantityOfDays: Int = 7
    var name = ""
    
    init(name: String) {
        self.name = name
    }
    
    var weekList: [String]  = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    
    
}
